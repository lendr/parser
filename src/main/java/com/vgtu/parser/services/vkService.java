package com.vgtu.parser.services;

import com.vgtu.parser.entities.Group;
import com.vgtu.parser.entities.Image;
import com.vgtu.parser.entities.Post;
import com.vgtu.parser.repositories.GroupRepository;
import com.vgtu.parser.repositories.ImageRepository;
import com.vgtu.parser.repositories.PostRepository;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import com.vk.api.sdk.objects.groups.GroupFull;
import com.vk.api.sdk.objects.photos.PhotoSizes;
import com.vk.api.sdk.objects.wall.WallpostAttachment;
import com.vk.api.sdk.objects.wall.WallpostAttachmentType;
import com.vk.api.sdk.objects.wall.WallpostFull;
import com.vk.api.sdk.objects.wall.responses.GetResponse;
import net.sourceforge.tess4j.Tesseract;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

@Service
public class vkService {
    UserActor actor;
    VkApiClient vkApiClient;
    String resourceFile;
    Properties prop;

    @Autowired
    PostRepository postRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    ImageRepository imageRepository;

    public vkService() throws IOException {
        resourceFile = getClass().getClassLoader().getResource("common.properties").getFile();
        prop = new Properties();
        prop.load(new FileInputStream(resourceFile));
    }

    public void saveCode(String code) throws IOException {
        prop.setProperty("code",code);
        prop.store(new FileOutputStream(resourceFile),null);

    }

    private String getFirstLinkFromPost(WallpostFull wallpostFull) {
        if (wallpostFull.getAttachments() == null) {
            return null;
        }
        for (WallpostAttachment attachment : wallpostFull.getAttachments()) {
            if (attachment.getType().equals(WallpostAttachmentType.LINK)) {
                String link = attachment.getLink().getUrl().toString(); // get only first link from post
                System.out.println("Link: " + link);
                return link;
            }
        }

        return null;
    }

    public void getPosts(String domain, Integer count) throws Exception {
        checkActor();
        Group group = saveGroup(domain);
        GetResponse groupWall = vkApiClient.wall().get(actor)
                .domain(domain)
                .count(count)
                .execute();

        for (WallpostFull wallpostFull : groupWall.getItems()) {
            Post post = new Post();
            post.setGroup(group);
            post.setPostId(wallpostFull.getId());
            post.setText(wallpostFull.getText());
            post.setLink(getFirstLinkFromPost(wallpostFull));
            postRepository.save(post);

            if (wallpostFull.getAttachments() == null)
                continue;
            wallpostFull.getAttachments().stream()
                    .filter(wallpostAttachment -> wallpostAttachment.getType().equals(WallpostAttachmentType.PHOTO))
                    .forEach(wallpostAttachment -> {
                        Optional<PhotoSizes> biggestPhoto = wallpostAttachment.getPhoto().getSizes().stream()
                                .sorted(Comparator.comparingInt(PhotoSizes::getHeight).reversed())
                                .findFirst();
                        if (biggestPhoto.isPresent()) {
                            PhotoSizes photo = biggestPhoto.get();
                            URL url = photo.getUrl();
                            try {
                                String[] split = url.getFile().split("/");
                                String name = split[split.length - 1];
                                String path = "src/main/resources/images/" + name;
                                FileUtils.copyURLToFile(url, new File(path));
                                Image newImage = new Image();
                                newImage.setName(name);
                                newImage.setPost(post);
                                newImage.setText(parseImage(path));
                                newImage = imageRepository.save(newImage);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            postRepository.save(post);
        }
    }

    private String parseImage(String path) {
        try {
            Tesseract tesseract = new Tesseract();
            BufferedImage bufferedImage = ImageIO.read(new File(path));
            File tessDataFolder = new File(prop.getProperty("tessdata.path"));
            tesseract.setDatapath(tessDataFolder.getAbsolutePath());
            tesseract.setLanguage(prop.getProperty("tessdata.language"));
            return tesseract.doOCR(bufferedImage);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    private void loadActorByCode(String code) throws Exception {
        TransportClient transportClient = HttpTransportClient.getInstance();
        vkApiClient = new VkApiClient(transportClient);
        UserAuthResponse response = vkApiClient.oAuth().userAuthorizationCodeFlow(Integer.valueOf(prop.getProperty("client.id")),
                prop.getProperty("client.secret"),
                prop.getProperty("redirect.uri"),
                code).execute();
        actor = new UserActor(response.getUserId(), response.getAccessToken());

    }

    private String getCode() {
        try {
            prop.load(new FileInputStream(resourceFile));
            return prop.getProperty("code");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void checkActor() throws Exception {
        if (actor == null || vkApiClient == null) {
            String code = getCode();
            if (code != null)
                loadActorByCode(code);
            else
                throw new RuntimeException("Please Sign in vk.com");
        }
    }

    private Group saveGroup(String domain) throws ApiException, ClientException {
        List<GroupFull> groupResponse = vkApiClient.groups().getById(actor).groupId(domain).execute();
        Integer groupId = groupResponse.get(0).getId();
        if (groupRepository.existsById(groupId)) {
            Optional<Group> byId = groupRepository.findById(groupId);
            return byId.get();
        } else {
            Group group = new Group();
            group.setGroupId(groupId);
            group.setName(groupResponse.get(0).getName());
            Group save = groupRepository.save(group);
            return save;
        }
    }
}
