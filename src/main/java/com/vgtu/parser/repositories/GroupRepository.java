package com.vgtu.parser.repositories;

import com.vgtu.parser.entities.Group;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepository extends CrudRepository<Group, Integer> {
}
