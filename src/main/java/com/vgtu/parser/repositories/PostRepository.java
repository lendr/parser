package com.vgtu.parser.repositories;

import com.vgtu.parser.entities.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post,Integer> {
}
