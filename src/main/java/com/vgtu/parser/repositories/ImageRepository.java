package com.vgtu.parser.repositories;

import com.vgtu.parser.entities.Image;
import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image,Integer> {
}
