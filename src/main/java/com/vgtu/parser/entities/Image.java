package com.vgtu.parser.entities;

import lombok.Data;

import javax.persistence.*;

@Entity(name = "vk_images")
@Data
public class Image {
    @Id
    @GeneratedValue
    Integer imageId;
    @ManyToOne
    @JoinColumn
    Post post;

    String name;
    @Column(length=10485760)
    String text;
}
