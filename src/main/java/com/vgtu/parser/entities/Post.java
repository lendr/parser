package com.vgtu.parser.entities;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity(name = "vk_posts")
@Data
public class Post {
    @Id
    Integer postId;
    @ManyToOne
    @JoinColumn()
    Group group;
    @OneToMany(mappedBy = "post")
    List<Image> images;
    @Column(length=10485760)
    String text;

    @Column
    String link;

}
