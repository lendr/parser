package com.vgtu.parser.entities;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity(name = "vk_groups")
@Data
public class Group {
    @Id
    Integer groupId;

    String name;

    @OneToMany(mappedBy = "group")
    List<Post> posts;
}
