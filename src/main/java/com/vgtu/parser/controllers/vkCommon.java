package com.vgtu.parser.controllers;

import com.vgtu.parser.repositories.PostRepository;
import com.vgtu.parser.services.vkService;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@RestController()
@RequestMapping("/test")
public class vkCommon {

    @Autowired
    vkService vkService;

    @Autowired
    PostRepository postRepository;

    @PostMapping("/image")
    public String tryParseImage(@RequestParam("file") MultipartFile file,
                                @RequestParam("language") String language) throws IOException, TesseractException {
        Tesseract tesseract = new Tesseract();
        BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
        File tessDataFolder = new File("src/main/resources/tessdata");
        tesseract.setDatapath(tessDataFolder.getAbsolutePath());
        tesseract.setLanguage(language);
        return tesseract.doOCR(bufferedImage);
    }

    @GetMapping("static")
    public String getString() {
        return "success";
    }

//    @GetMapping("auth")
//    public void authorize() throws IOException {
//        String str = "https://oauth.vk.com/authorize?client_id=7190553&response_type=code&scope=262144&redirect_uri=http://localhost:8080/test/vk/auth";
//        HttpClient client = new DefaultHttpClient();
//        HttpGet request = new HttpGet(str);
//        HttpResponse response = client.execute(request);
//    }

    @GetMapping("vk/auth")
    public void saveAuthDate(@RequestParam("code") String code) throws ClientException, ApiException, IOException {
        vkService.saveCode(code);
    }

    @GetMapping("vk/{domain}")
    public void getPosts(@PathVariable("domain") String domain,
                         @RequestParam(value = "count", defaultValue = "15") Integer count) throws Exception {
        vkService.getPosts(domain, count);
    }

    @GetMapping("vk/public/{name}")
    public void fetchDataFromVkPublic(@PathVariable("name") String publicName) {

        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vkApiClient = new VkApiClient(transportClient);
//        vkApiClient.oAuth().userAuthorizationCodeFlow(7190553,"YUmLVXqH0uySS6OtG9eE",null,code)

    }
}
